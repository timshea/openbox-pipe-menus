# README #

I've been using Openbox for quite some time, and finally got around to using pipe menus. 

Here I'll share the useful ones.

### What is this repository for? ###

* Various Openbox pipe menus.

### How do I get set up? ###

* Copy shell script to somewhere in your path
* Edit the shell script's configuration section
* No dependencies, should run on any system Openbox runs on, in any POSIX-compatible shell

### Contribution guidelines ###

* Please keep the following in mind when submitting patches:
  http://www.pixelbeat.org/programming/shell_script_mistakes.html
* Submit patches using 'diff -u'

### Who do I talk to? ###

* Tim Shea < timshea+bitbucket@gmail.com >

### If I've saved you some time, feel free to buy me a beer ###

[![](https://epay.propay.com/images/BuyButton/DonateNow.png)](https://epay.propay.com/external/buy.aspx?d=thePDePjW8QK5BKoStKRzZ1krXx57XhePMMOD2BEE5krs54O8A1nYb5DSGhmC3ICYrYI77YUvmqoBYTGt1fNizu19k5bG8I2lFTbFH%2fQ%2f5oVcsKOzaSMznesv6BrZbSbjaksxBKcvmXIczmyIICJRRu4MQ%3d%3d&a=31624641)
